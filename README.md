# README #

### How do I get set up? ###

* Summary of set up
 Small php project that exposes a rest api

* Dependencies
    If you are not using Homestead, you will need to make sure your server meets the following requirements:
    * PHP >= 7.1.3
    * OpenSSL PHP Extension
    * PDO PHP Extension
    * Mbstring PHP Extension
    * Tokenizer PHP Extension
    * XML PHP Extension
    * Ctype PHP Extension
    * JSON PHP Extension

* Configuration
     Have a webserver with a vhost that points to the public folder of the project.
     Have a database connection
     Have composer installed
     Run composer install command inside the project root
     Create the .env file inside the project root should look like:

        APP_ENV=local
        APP_KEY=
        APP_DEBUG=true
        APP_LOG_LEVEL=debug
        APP_URL=http://localhost

        DB_CONNECTION=
        DB_HOST=
        DB_PORT=
        DB_DATABASE=
        DB_USERNAME=
        DB_PASSWORD=

        YOUTUBE_API_KEY=

    Run php artisan key:generate  This will generate the APP_KEY in .env
    Set up the correct values for the keys in the .env file, db connection and youtube api key
    Add keywords into search_filter file
    Send requests to the routes.


* Available routes
    For the available routes please run
        php artisan route:list

        The api/videos route also supports a param called 'search' which it's used to filter the videos
        ex: http://psg.local/api/videos?search=something