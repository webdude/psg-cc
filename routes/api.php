<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/videos/populate', 'VideoController@populateDb');
Route::apiResource('videos', 'VideoController')->except([
    'store', 'update',
]);;

Route::fallback(function(){
    return response()->json(['error' => 'Route Not Found!'], 404);
});
