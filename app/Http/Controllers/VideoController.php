<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Channel;
use App\Models\Video;
use App\Http\Controllers\ChannelController;
use \Google_Client;
use \Google_Service_YouTube;
use Illuminate\Support\Facades\Validator;

class VideoController extends Controller
{

    private $filter = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $req)
    {
        $validator = Validator::make(request()->all(), [
            'q' => 'string',
        ]);

        if ($validator->fails())
            return response()->json(['error' => $validator->messages()], 404);

        $q = $req->query('search');

        if (!empty(trim($q))) {
            $condition = explode(' ', $q);

            $videos = Video::select('id', 'title')
                ->where('title', 'like', '%' . implode('%', $condition) . '%')
                ->get();
        } else {
            $videos = Video::all();
        }

        return response()->json($videos);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $validator = Validator::make(request()->route()->parameters, [
            'video' => 'required|int',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 404);
        } else {
            $video = Video::find($id);
            return $video ? $video : response()->json(['error' => "Video not found!"], 404);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $validator = Validator::make(request()->route()->parameters, [
            'video' => 'required|int',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 404);
        } else {
            $count = Video::destroy($id);
            return $count ? response()->json(['success' => 'Video deleted'], 200) : response()->json(['error' => 'Video not found!'], 404);
        }
    }

    public function populateDb(ChannelController $channelCtrl)
    {
        $channelIds = [];

        foreach ($channelCtrl->channelNames as $channelName) {

            $existingChannel = Channel::where('channel_name', $channelName)
                ->first();

            if (!$existingChannel) {
                $channel = new Channel;
                $channel->id = rand(0, 100000);
                $channel->channel_name = $channelName;
                $channel->save();
            }

            $channelYtId = $channelCtrl->getChannelYtId($channelName);
            $channelIds[] = $channelYtId;

        }

        $channelUploadIds = $channelCtrl->getChannelUploadIds($channelIds);

        $this->filter = $this->loadFilter();

        foreach ($channelUploadIds as $channelId => $playlistId) {
            $this->processVideos($playlistId);
        }

        return response()->json(['success' => "db populated"], 200);
    }

    private function processVideos($playlistId)
    {

        $client = new Google_Client();
        $client->setDeveloperKey(env('YOUTUBE_API_KEY'));
        $youtube = new Google_Service_YouTube($client);

        $nextPageToken = $this->filterVideos($youtube, $playlistId);

        while ($nextPageToken) {
            $nextPageToken = $this->filterVideos($youtube, $playlistId, $nextPageToken);
        }
    }

    private function loadFilter()
    {
        $filter = null;

        try {
            $search_filter = fopen("../search_filter", "r");
        } catch (\Exception $e) {
            \Log::error($e);
            return null;
        }

        if ($search_filter) {
            while (!feof($search_filter)) {
                $filter[] = trim(fgets($search_filter));
            }
            fclose($search_filter);
        }

        return $filter;
    }


    private function getPlayListVideos($youtube, $playlistId, $nextPageToken = null)
    {
        $part = 'snippet';
        $params = array('playlistId' => $playlistId);

        $nextPageToken ? ($params['pageToken'] = $nextPageToken) : null;

        try {
            $response = $youtube->playlistItems->listPlaylistItems(
                $part,
                $params
            );

            return $response;
        } catch (\Exception $e) {
            \Log::error($e);
            return null;
        }

    }

    private function filterVideos($youtube, $playlistId, $nextPageToken = null)
    {
        $response = $this->getPlayListVideos($youtube, $playlistId, $nextPageToken);

        foreach ($response->items as $videoYt) {

            $title = $videoYt->snippet->title;

            foreach ($this->filter as $filter) {
                $pass = true;
                $multiWord = explode(' ', strtolower($filter));

                foreach ($multiWord as $part) {
                    if (strstr(strtolower($title), $part) === false) {
                        $pass = false;
                        break;
                    };
                }

                if ($pass) {
                    $video = new Video;
                    $video->id = $this->getVideoId();
                    $video->title = $title;
                    $video->date = Carbon::createFromTimeString($videoYt->snippet->publishedAt);
                    $video->save();
                }
            }
        }

        return $response->nextPageToken ? $response->nextPageToken : null;
    }

    private function getVideoId()
    {
        $result = Video::select('id')
            ->orderBy('id', 'desc')
            ->limit(1)
            ->first();

        return isset($result->id) ? ($result->id + 1) : 1;
    }

}
