<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Google_Client;
use \Google_Service_YouTube;

class ChannelController extends Controller
{

    public $channelNames = ['GlobalCyclingNetwork', 'globalmtb'];

    public function getChannelUploadIds($channelIds)
    {
        $client = new Google_Client();
        $client->setDeveloperKey(env('YOUTUBE_API_KEY'));

        $youtube = new Google_Service_YouTube($client);

        $part = 'contentDetails';
        $params = array('id' => implode(',', $channelIds));

        $channelUploadIds = [];

        try {
            $response = $youtube->channels->listChannels(
                $part,
                $params
            );

            if (isset($response->items)) {
                foreach ($response->items as $channel) {
                    if (isset($channel->contentDetails->relatedPlaylists->uploads)) {
                        $channelUploadIds[$channel->id] = $channel->contentDetails->relatedPlaylists->uploads;
                    }
                }
            }

            return $channelUploadIds;

        } catch (\Exception $e) {
            \Log::error($e);
            return null;
        }
    }

    public function getChannelYtId($channelName)
    {
        $client = new Google_Client();
        $client->setDeveloperKey(env('YOUTUBE_API_KEY'));
        $youtube = new Google_Service_YouTube($client);

        $part = 'snippet';
        $params = array('maxResults' => 1, 'q' => $channelName, 'type' => 'channel');

        try {
            $response = $youtube->search->listSearch(
                $part,
                $params
            );

            return isset($response->items[0]->id->channelId) ? $response->items[0]->id->channelId : null;

        } catch (\Exception $e) {
            \Log::error($e);
            return null;
        }

    }

}
